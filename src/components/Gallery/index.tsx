import React from 'react'
import { getMovieList, getMovieGenreList } from '../../axios'
import { Tabs, Modal, Icon, message } from 'antd';
import error from '../../assets/img/error.svg';
const { TabPane } = Tabs;

class Gallery extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            page: 1,
            pagesize: 10,
            type: [],
            imgUrl: null,
            api_key: '78eb203a82c95643e4094d63e86a1750',
            visible: false,
            movieItem: null,
            index: 0
        }

    }

    componentDidMount() {
        this.init();
    }

    componentWillUnmount() {
        this.setState = (state, callback) => { return; };
    }

    init = () => {
        let config: string | any
        config = localStorage.getItem('config')
        let imgUrl = JSON.parse(config).images.base_url
        this.setState({
            imgUrl: imgUrl + 'original'
        })
        this.getMovieList(null)
        this.getMovieGenreList()

    }

    getMovieGenreList = () => {
        let data = { api_key: this.state.api_key }
        getMovieGenreList(data).then((res: any) => {
            let all = [{ id: 'all', name: 'All' }]
            this.setState({
                type: all.concat(res.genres)
            })
        })
    }

    getMovieList = (parms: any) => {
        let data = { api_key: this.state.api_key, page: this.state.page }
        console.log("🚀 ~ file: index.tsx ~ line 65 ~ Gallery ~ this.state.page", this.state.page)
        getMovieList({ ...data, ...parms }).then((res: any) => {
            let stop = this.state.page > res.total_pages ? false : true
            this.setState({
                data: this.state.data.concat(res.results),
                page: this.state.page + 1
            }, () => {
                if (this.state.page < 6 && stop) {
                    this.getMovieList(parms)
                } else {
                    this.setState({
                        page: 1,
                    })
                }
            })
        })
    }

    onChange = (e: any) => {
        let with_genres = { with_genres: e }
        this.setState({
            data: [],
            page: 1,
            idList: []
        }, () => {
            if (e === 'all') {
                this.getMovieList(null)
            } else {
                this.getMovieList(with_genres)
            }
        })

    }


    showDetail = (item: any, index: any) => {
        this.setState({
            movieItem: item,
            visible: true,
            index,
        })
    }

    handleCancel = () => {
        this.setState({
            visible: false,
            movieItem: null,
            index: 0
        })
    }

    Previous = () => {
        const { index, data } = this.state
        let i: any
        i = index - 1
        if (i < 0) {
            message.warning("It's over")
        } else {
            let item = data[i]
            this.setState({
                movieItem: item,
                index:i
            })
            console.log("item",item);
        }

    }

    Next = () => {
        const { index, data } = this.state
        let i: any
        i = index + 1
        if (i > data.length) {
            message.warning("It's over")
        } else {
            let item = data[i]
            this.setState({
                movieItem: item,
                index:i
            })
            console.log("item",item);
        }
    }

    render() {
        const { data, imgUrl, type, visible, movieItem } = this.state
        return (
            <div>
                <Tabs defaultActiveKey="all" tabPosition={'top'} onChange={this.onChange}>
                    {type.map((i: any) => (
                        <TabPane tab={i.name} key={i.id}>
                        </TabPane>
                    ))}
                </Tabs>
                <div className="list">
                    {data.map((item: any, index: any) => {
                        return <div className='imgbox' onClick={() => { this.showDetail(item, index) }} key={item.id}> <img alt={item.title}  src={imgUrl + item.poster_path} /></div>

                    })
                    }
                </div>
                {visible && <Modal
                    title="Basic Modal"
                    className="modelDetail"
                    visible={visible}
                    onCancel={this.handleCancel}
                    width={720}
                    centered
                    footer={[]}
                >
                    <div className='modelBox'>
                        <div className="modelBtn">
                            <span onClick={() => { this.Previous() }}><Icon type="left-circle" theme="filled" /></span>
                            <span onClick={() => { this.Next() }}> <Icon type="right-circle" theme="filled" /></span>
                        </div>
                        <div className='contentBox'>
                            <div><img alt={movieItem.title} src={movieItem.backdrop_path ? imgUrl + movieItem.backdrop_path : error} /></div>
                            <div>
                                <h2>{movieItem.title + '(' + movieItem.vote_average + '  Vote Average)'}</h2>
                                <h4>{"Release Date:  " + movieItem.release_date}</h4>
                                <h6>{movieItem.overview}</h6>
                            </div>
                        </div>
                    </div>
                </Modal>}
            </div>
        );
    }
}

export default Gallery