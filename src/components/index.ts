// 路由懒加载
import { lazy } from "react";

const Home = lazy(() => import('./Home'))
const Gallery = lazy(() => import('./Gallery'))
const Search= lazy(() => import('./Search'))
export {
    Home,
    Gallery,
    Search
}