import React from 'react'
import { getMovieList, getMovieSearchList } from '../../axios'
import { Select, Card, Form, Input, Radio, Modal, Icon, message } from 'antd';
import error from '../../assets/img/error.svg';
const { Meta } = Card;
const { Option } = Select;
class Search extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            page: 1,
            pagesize: 10,
            imgUrl: null,
            api_key: '78eb203a82c95643e4094d63e86a1750',
            visible: false,
            movieItem: null,
            index: 0,
            loading: true,
            radioValue: 'asc',
            sortBy: 'original_title'
        }

    }

    componentDidMount() {
        this.init();
    }

    componentWillUnmount() {
        this.setState = (state, callback) => { return; };
    }

    init = () => {
        let config: string | any
        config = localStorage.getItem('config')
        let imgUrl = JSON.parse(config).images.base_url
        this.setState({
            imgUrl: imgUrl + 'original'
        })
        this.getMovieList(null)
    }

    getMovieList = (parms: any) => {
        let data = { api_key: this.state.api_key, page: this.state.page }
        console.log("🚀 ~ file: index.tsx ~ line 65 ~ Gallery ~ this.state.page", this.state.page)
        getMovieList({ ...data, ...parms }).then((res: any) => {
            let stop = this.state.page > res.total_pages ? false : true
            this.setState({
                data: this.state.data.concat(res.results),
                page: this.state.page + 1,
                loading: false
            }, () => {
                if (this.state.page < 6 && stop) {
                    this.getMovieList(parms)
                } else {
                    this.setState({
                        page: 1
                    })
                }
            })
        })
    }

    getMovieSearchList = (data: any) => {
        var obj = { 'query': data, api_key: this.state.api_key }
        getMovieSearchList(obj).then((res: any) => {
            this.setState({
                data: res.results,
                page: 1,
                loading: false
            }, () => {
                this.sortByTitle('asc')
            })
        })

    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            console.log("🚀 ~ file: index.tsx ~ line 73 ~ Search ~ this.props.form.validateFields ~ values", values)
            let data = { sort_by: values.sort_by + '.' + values.order, "primary_release_year": values.primary_release_year }
            this.setState({
                data: [],
                page: 1
            }, () => {
                this.getMovieList(data)
            })
        });
    };

    showDetail = (item: any, index: any) => {
        this.setState({
            movieItem: item,
            visible: true,
            index,
        })
    }

    handleCancel = () => {
        this.setState({
            visible: false,
            movieItem: null,
            index: 0
        })
    }

    Previous = () => {
        const { index, data } = this.state
        let i: any
        i = index - 1
        if (i < 0) {
            message.warning("It's over")
        } else {
            let item = data[i]
            this.setState({
                movieItem: item,
                index: i
            })
            console.log("item", item);
        }

    }

    Next = () => {
        const { index, data } = this.state
        let i: any
        i = index + 1
        if (i > data.length) {
            message.warning("It's over")
        } else {
            let item = data[i]
            this.setState({
                movieItem: item,
                index: i
            })
            console.log("item", item);
        }
    }

    onInputChange = (e: any) => {
        const { value } = e.target;
        console.log("🚀 ~ file: index.tsx ~ line 144 ~ Search ~ value", value)
        if(value){
            this.getMovieSearchList(value)
        }else{
            this.setState({
                data:[],
                page:1
            },()=>{
                this.init()
            })
        }
    }

    onSelectChange = (value: any) => {
        this.setState({
            sortBy: value
        })
        if (value === 'original_title') {
            this.sortByTitle(this.state.radioValue)
        } else {
            this.sortByVote(this.state.radioValue)
        }
    }

    // 姓名排序
    sortByTitle = (type: any) => {
        let newData: any = []
        if (type === 'asc') {
            newData = this.state.data.sort(
                function (a: any, b: any) {//从小到大排序
                    if (a.title < b.title) return -1;//位置不变
                    else return 1;//交换位置
                }
            )
        }
        else {
            newData = this.state.data.sort(
                function (a: any, b: any) {//从小到大排序
                    if (a.title > b.title) return -1;//位置不变
                    else return 1;//交换位置
                }
            )
        }
        this.setState({
            data: [...newData]
        })
    }

    // 评分排序
    sortByVote = (type: any) => {
        let newData: any = []
        if (type === 'asc') {
            newData = this.state.data.sort(
                function (a: any, b: any) {//从小到大排序
                    if (a.vote_average < b.vote_average) return -1;//位置不变
                    else return 1;//交换位置
                }
            )
        }
        else {
            newData = this.state.data.sort(
                function (a: any, b: any) {//从大到小排序
                    if (a.vote_average > b.vote_average) return -1;//位置不变
                    else return 1;//交换位置
                }
            )
        }
        this.setState({
            data: [...newData]
        })
    }

    onRadioChange = (e: any) => {
        const { value } = e.target;
        this.setState({
            radioValue: value
        })
        if (this.state.sortBy === 'original_title') {
            this.sortByTitle(value)
        } else {
            this.sortByVote(value)
        }
    }

    render() {
        const { data, imgUrl } = this.state
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 7 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 17 },
            },
        };
        const { getFieldDecorator } = this.props.form;
        // eslint-disable-next-line
        const { visible, movieItem, loading, radioValue } = this.state
        return (
            <div className='search-box'>
                <div className='search'>
                    <Form {...formItemLayout}>
                        <Form.Item label="Title">
                            <Input allowClear onChange={this.onInputChange} />
                        </Form.Item>
                        <Form.Item label="Sort by">
                            {getFieldDecorator('sort_by', {
                                initialValue: 'original_title',
                            })(
                                <Select onChange={this.onSelectChange}>
                                    <Option value="original_title">Title</Option>
                                    <Option value="vote_average">Vote Average</Option>
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Order">
                            {getFieldDecorator('order', {
                                initialValue: radioValue,
                            })(
                                <Radio.Group onChange={this.onRadioChange}>
                                    <Radio value="asc">Ascending</Radio>
                                    <Radio value="desc">Descending</Radio>
                                </Radio.Group>
                            )}
                        </Form.Item>
                        {/* <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
                            <Button type="primary" htmlType="submit" >
                                Submit
                            </Button>
                        </Form.Item> */}
                    </Form>
                </div>
                <div className="list">

                    {data.map((item: any, index: any) => {
                        return <Card
                            onClick={() => { this.showDetail(item, index) }}
                            hoverable
                            key={item.id}
                            cover={<img alt="example" src={item.poster_path ? imgUrl + item.poster_path : error} />}
                        >
                            <Meta title={item.title} description={"Vote Average:" + item.vote_average + '     ' + item.release_date} />
                        </Card>
                    })
                    }

                </div>
                {visible && <Modal
                    title="Basic Modal"
                    className="modelDetail"
                    visible={visible}
                    onCancel={this.handleCancel}
                    width={720}
                    centered
                    footer={[]}
                >
                    <div className='modelBox'>
                        <div className="modelBtn">
                            <span onClick={() => { this.Previous() }}><Icon type="left-circle" theme="filled" /></span>
                            <span onClick={() => { this.Next() }}> <Icon type="right-circle" theme="filled" /></span>
                        </div>
                        <div className='contentBox'>
                            <div><img alt={movieItem.title} key={movieItem.id} src={movieItem.backdrop_path ? imgUrl + movieItem.backdrop_path : error} /></div>
                            <div>
                                <h2>{movieItem.title + '(' + movieItem.vote_average + '  Vote Average)'}</h2>
                                <h4>{"Release Date:  " + movieItem.release_date}</h4>
                                <h6>{movieItem.overview}</h6>
                            </div>
                        </div>
                    </div>
                </Modal>}

            </div>
        );
    }
}

export default Form.create({ name: "serach" })(Search)
