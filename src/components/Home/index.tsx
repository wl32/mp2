

import { Layout, Breadcrumb, Menu,Spin } from 'antd';
import * as React from 'react';
import { withRouter } from 'react-router-dom';
import Gallery from '../Gallery'
import Search from '../Search'
import { getConfig } from '../../axios'
const { Header, Content, Footer } = Layout;
// @ts-ignore
@withRouter
class Home extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            current: '1',
            loading:true
        }

    }

    componentDidMount() {
        this.getConfig()
    }

    componentWillUnmount() {
        this.setState = (state, callback) => { return; };
    }

    getConfig = () => {
        let data = { api_key: '78eb203a82c95643e4094d63e86a1750'}
        getConfig(data).then((res:any)=>{
             localStorage.setItem('config',JSON.stringify(res))
             this.setState({
                 loading:false
             })
        })
    }

    handleClick = (e: any) => {
        console.log('click ', e);
        this.setState({
            current: e.key,
        });
    };

    render() {
        const { current,loading } = this.state
        return (
            <div className="box">
                  <Spin spinning={loading}>
                <Layout className="layout">
                    <Header>
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['1']}
                            style={{ lineHeight: '64px' }}
                            onClick={this.handleClick}
                        >
                            <Menu.Item key="1">Gallery</Menu.Item>
                            <Menu.Item key="2">Search</Menu.Item>

                        </Menu>
                    </Header>
                    <Content style={{ padding: '0 50px' }}>
                        <Breadcrumb style={{ margin: '16px 0' }}>
                            <Breadcrumb.Item>Home</Breadcrumb.Item>
                            <Breadcrumb.Item> { current === '1' ? "Gallery" : "Search"}</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="pageContent">
                            {current === '1' && <Gallery></Gallery>}
                            {current === '2' && < Search></ Search>}
                        </div>
                    </Content>
                    <Footer className='foot'>Millions of movies, TV shows and people to discover. Explore now.</Footer>
                </Layout>
                </Spin>
            </div>
        );
    }
}

export default Home