import React from 'react'
//  Redirect,
import { Route, Switch, withRouter } from 'react-router-dom'
import { Home } from './components'
import { Routers } from './routers'

function App () {
  return (
      <Switch>
        {
          Routers.map(router => (
            <Route
              exact={!router.notExect}
              key={router.path}
              path={router.path}
              component={router.component}
            >
            </Route>
          ))
        }
        <Route path="/" component={Home} exact></Route>
      </Switch>
  )
}

export default withRouter(App)