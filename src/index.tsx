import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import App from './App'
// 设置常规样式
import "normalize.css"
// 引入全局样式
import "./assets/styles/core.less"
import 'antd/dist/antd.css'

import { BrowserRouter, Route, Switch } from 'react-router-dom'
ReactDOM.render(
  <BrowserRouter basename ="/mp2">
    {/* 使用了路由懒加载，所以需要使用<Suspense>包起来 */}
    <Suspense fallback={<div></div>}>
      <Switch>
        <Route path="/" render={routerProps => {
          return <App {...routerProps}/>
        }}/>
      </Switch>
    </Suspense>
  </BrowserRouter>,
  document.getElementById('root')
)
