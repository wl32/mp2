import { Gallery } from "../components";

export const baseUrl = {
    list : '/gallery'
}

const baseRouters = [
    {
        path:baseUrl.list,
        component:Gallery,
        root:[]
    }
]

export default baseRouters