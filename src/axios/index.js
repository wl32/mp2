
import { get} from './tools';
import * as config from './config';



// MOVIE_CONFIGURATION
export const getConfig = (data) => get({
    url: config.MOVIE_CONFIGURATION,
    params:data
})
// MOVIE_LIST
export const getMovieList = (data) => get({
    url: config.MOVIE_LIST,
    params:data
})
// MOVIE_GENRE_LIST
export const getMovieGenreList = (data) => get({
    url: config.MOVIE_GENRE_LIST,
    params:data
})

// MOVIE_SEARCH_LIST
export const getMovieSearchList = (data) => get({
    url: config.MOVIE_SEARCH_LIST,
    params:data
})
