/**
 * http通用工具函数
 */
import axios from 'axios';
import { message } from 'antd';
/**
 * 公用get请求
 * @param url       接口地址
 * @param msg       接口异常提示
 * @param headers   接口所需header配置
 */
export const get = ({ url, params, msg = 'Interface exception', headers }) =>
    axios.get(url, { params }, headers).then(res => res.data).catch(err => {
        message.warn(msg);
        throw err;
    });